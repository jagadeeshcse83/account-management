package com.jaga.customerservice.validator;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.jaga.customerservice.dto.AccountCustomerDTO;
import com.jaga.customerservice.dto.ReturnHolder;
import com.jaga.customerservice.model.Customer;
import com.jaga.customerservice.repository.CustomerRepo;
import com.jaga.customerservice.repository.RoleRepo;

@Component
public class CustomerValidtor implements Validator{

	@Autowired RoleRepo roleRepo;
	
	@Autowired CustomerRepo customerRepo;
	
	List<String> roles = Arrays.asList("BRANCH_MANAGER","CUSTOMER");
	
	List<String> gender = Arrays.asList("M","F", "O");

	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		
	}

	public ReturnHolder validateCustomer(AccountCustomerDTO accountCustomerDTO, String customerId) {
		ReturnHolder returnHolder = null;
		if (accountCustomerDTO == null) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid Data");
		} else if (accountCustomerDTO.getRoleCode() == null || !roles.contains(accountCustomerDTO.getRoleCode())) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid role specfied. Valid roles are " + roles);
		} else if (accountCustomerDTO.getDateOfBirth() == null) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid date of birth");
		} else if (!isvalidDate(accountCustomerDTO.getDateOfBirth())) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid date formate for dob. valid format dd/MM/yyyy ");
		} else if (accountCustomerDTO.getGender() == null || gender.contains(accountCustomerDTO.getGender())) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid gender. Valid genders are " + gender);
		} else if (accountCustomerDTO.getUserName() == null || accountCustomerDTO.getUserName().length() <= 3) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid user name");
		} else if (customerId == null && customerRepo.findCustomerByName(accountCustomerDTO.getUserName()) != null) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "user name already existed in the system");
		} else if (customerId != null) {
			Optional<Customer> customer = customerRepo.findById(customerId);
			if (!customer.isPresent()) {
				return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "customer not found in the system");
			}
			Customer existingCustomer = customerRepo.findCustomerByName(accountCustomerDTO.getUserName());
			if (existingCustomer != null && !existingCustomer.getUserId().equals(customerId)) {
				return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "user name already existed in the system");
			} else {
				return returnHolder;
			}
		} else {
			return returnHolder;
		}
	}
	
	private boolean isvalidDate(String dateOfBirth) {
		try {
			new SimpleDateFormat("dd/MM/yyyy").parse(dateOfBirth);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
}
