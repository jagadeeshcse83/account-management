package com.jaga.customerservice.dto;

import java.io.Serializable;

public class AccountCustomerDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String accountType;
	
	private String openDate;
	
	private String dateOfBirth;
	
	private String phoneNumber;
	
	private String userName;
	
	private Character gender;
	
	private String branch;
	
	private String roleCode;
	
	private String pass;

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Character getGender() {
		return gender;
	}

	public void setGender(Character gender) {
		this.gender = gender;
	}
	
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "AccountCustomerDTO [accountType=" + accountType + ", openDate=" + openDate + ", dateOfBirth="
				+ dateOfBirth + ", phoneNumber=" + phoneNumber + ", userName=" + userName + ", gender=" + gender
				+ ", branch=" + branch + ", roleCode=" + roleCode + ", pass=" + pass + "]";
	}

}
