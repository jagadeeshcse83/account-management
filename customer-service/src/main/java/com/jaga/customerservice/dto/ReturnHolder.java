package com.jaga.customerservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({ "statusCode", "result", "message" })
public class ReturnHolder {

	@JsonProperty("message")
	private String message;
	@JsonProperty("result")
	private Object result;
	@JsonProperty("statusCode")
	private Integer statusCode = 200;

	public ReturnHolder() {

	}

	public ReturnHolder(Object result) {
		super();
		this.result = result;
	}

	public ReturnHolder(Integer statusCode, String message) {
		super();
		this.statusCode = statusCode;
		this.message = message;
	}

	public ReturnHolder(String message, Object result, Integer statusCode) {
		super();
		this.message = message;
		this.result = result;
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "ReturnHolder [message=" + message + ", result=" + result + ", statusCode=" + statusCode + "]";
	}

}
