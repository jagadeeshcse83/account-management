package com.jaga.customerservice.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jaga.customerservice.model.Role;

@Repository
public interface RoleRepo extends CrudRepository<Role, String>{

	@Query(value = "SELECT * FROM role r WHERE r.role_code = ?1", nativeQuery = true)
	Role findByRoleCode(String roleCode);
	
}
