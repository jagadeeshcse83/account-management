package com.jaga.customerservice.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.jaga.customerservice.model.Customer;

@Repository
public interface CustomerRepo extends PagingAndSortingRepository<Customer, String>{

	@Query(value = "SELECT * FROM customer c WHERE c.user_name = ?1", nativeQuery = true)
	Customer findCustomerByName(String userName);

}
