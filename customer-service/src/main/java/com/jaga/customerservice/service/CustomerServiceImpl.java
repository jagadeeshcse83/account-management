package com.jaga.customerservice.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jaga.customerservice.dto.AccountCustomerDTO;
import com.jaga.customerservice.dto.ReturnHolder;
import com.jaga.customerservice.model.Customer;
import com.jaga.customerservice.model.Role;
import com.jaga.customerservice.repository.CustomerRepo;
import com.jaga.customerservice.repository.RoleRepo;
import com.jaga.customerservice.validator.CustomerValidtor;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired RoleRepo roleRepo;
	
	@Autowired CustomerRepo customerRepo;
	
	@Autowired CustomerValidtor custValidtor;
	
	@Autowired PasswordEncoder bcryptEncoder;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Override
	public ReturnHolder createCustomer(AccountCustomerDTO accountCustomerDTO) {
		ReturnHolder returnHolder = custValidtor.validateCustomer(accountCustomerDTO, null);
		if (returnHolder == null) {
			return createOrUpdateCustomer(accountCustomerDTO, null);
		} else {
			return returnHolder;
		}
	}

	@Override
	public ReturnHolder update(final AccountCustomerDTO accountCustomerDTO, final String customerId) {
		ReturnHolder returnHolder = custValidtor.validateCustomer(accountCustomerDTO, customerId);
		if (returnHolder == null) {
			return createOrUpdateCustomer(accountCustomerDTO, customerId);
		} else {
			return returnHolder;
		}
	}

	@Override
	public ReturnHolder delete(String customerId) {
		if (customerId != null) {
			Optional<Customer> customer = customerRepo.findById(customerId);
			if (customer.isPresent()) {
				customerRepo.delete(customer.get());
				return new ReturnHolder(HttpStatus.OK.value(), "customer deleted successfully");
			} else {
				return new ReturnHolder(HttpStatus.BAD_REQUEST.value(),"customer not found in the system");
			}
		} else {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(),"Invalid customer id ");
		}
	}

	@Override
	public ReturnHolder getAllCustomers(final Integer pageNo,final Integer pageSize,final String sortBy) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Collection<? extends GrantedAuthority> roles = authentication.getAuthorities();
		boolean isBankManager = Boolean.FALSE;
		logger.debug("rles"+roles);	
		for (GrantedAuthority grantedAuthority : roles) {
			if (grantedAuthority.getAuthority().equalsIgnoreCase("BRANCH_MANAGER"))
			isBankManager = Boolean.TRUE;
		}
		if (isBankManager) {
			Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
	        Page<Customer> pagedResult = customerRepo.findAll(paging);
	        if(pagedResult.hasContent()) {
	        	return new ReturnHolder("", pagedResult.getContent(), HttpStatus.OK.value());
	        } else {
	        	return new ReturnHolder("No results found", new ArrayList<>(), HttpStatus.OK.value());
	        }
		} else {
			return new ReturnHolder("User don't have access to other records", customerRepo.findCustomerByName(authentication.getName()), HttpStatus.OK.value());
		}
	}

	/**
	 * A common method to create and update the customer
	 * @param accountCustomerDTO
	 * @param customerId
	 * @return
	 */
	private ReturnHolder createOrUpdateCustomer(AccountCustomerDTO accountCustomerDTO, String customerId) {
		try {
			Role role = roleRepo.findByRoleCode(accountCustomerDTO.getRoleCode());
			Date date = null;
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(accountCustomerDTO.getDateOfBirth());
			} catch (ParseException e) {
				return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid date formate for dob. valid format dd/MM/yyyy ");
			}
			UUID randomUUID = UUID.randomUUID();
			Customer customer = new Customer(customerId == null ? randomUUID.toString() : customerId, accountCustomerDTO.getUserName(), date,
					accountCustomerDTO.getGender(), accountCustomerDTO.getPhoneNumber(), role);
			if (customerId == null) {
				customer.setPassword(accountCustomerDTO.getPass() != null ? bcryptEncoder.encode(accountCustomerDTO.getPass()) : bcryptEncoder.encode(RandomStringUtils.randomAlphanumeric(6)));
			}
			Customer customerInfo = customerRepo.save(customer);
			if (customerId == null) {
				return new ReturnHolder("customer created successfully", customerInfo, HttpStatus.OK.value());
			} else {
				return new ReturnHolder(HttpStatus.OK.value(), "customer updated successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ReturnHolder(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong ");
		}
	}

	@Override
	public ReturnHolder getCustomerByName(String userName) {
		Customer customerInfo = customerRepo.findCustomerByName(userName);
		return new ReturnHolder("", customerInfo, HttpStatus.OK.value());
	}
}
