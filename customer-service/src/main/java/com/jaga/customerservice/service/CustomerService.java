package com.jaga.customerservice.service;

import com.jaga.customerservice.dto.AccountCustomerDTO;
import com.jaga.customerservice.dto.ReturnHolder;

public interface CustomerService {

	ReturnHolder createCustomer(final AccountCustomerDTO accountCustomerDTO);
	
	ReturnHolder update(final AccountCustomerDTO accountCustomerDTO, final String customerId);
	
	ReturnHolder delete(final String customerId);
	
	ReturnHolder getAllCustomers(final Integer pageNo,final Integer pageSize,final String sortBy);

	ReturnHolder getCustomerByName(final String userName);
}
