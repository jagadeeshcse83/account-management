package com.jaga.customerservice.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.customerservice.config.JwtTokenUtil;
import com.jaga.customerservice.dto.JwtRequest;
import com.jaga.customerservice.dto.JwtResponse;
import com.jaga.customerservice.dto.ReturnHolder;
import com.jaga.customerservice.model.Customer;
import com.jaga.customerservice.repository.CustomerRepo;
import com.jaga.customerservice.repository.RoleRepo;
import com.jaga.customerservice.service.JwtUserDetailsService;


@RestController
@CrossOrigin
public class SampleController {

	@Autowired AuthenticationManager authenticationManager;

	@Autowired JwtTokenUtil jwtTokenUtil;

	@Autowired JwtUserDetailsService userDetailsService;
	
	@Autowired RoleRepo roleRepo;
	
	@Autowired CustomerRepo customerRepo;
	
	@Autowired PasswordEncoder bcryptEncoder;
	
	
	private static final Logger logger = LoggerFactory.getLogger(SampleController.class);

	@RequestMapping(value = "/generate-token", method = RequestMethod.POST)
	public ReturnHolder createAuthenticationToken(@RequestBody JwtRequest authenticationRequest){

		try {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		} catch (Exception e) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(),"Invalid username or password");
		}

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);

		return new ReturnHolder("token generated ",new JwtResponse(token),HttpStatus.OK.value());
	}
	
	@RequestMapping(value = "/test-customer", method = RequestMethod.GET)
	public ReturnHolder createTestUser() {
		Customer customer = new Customer(UUID.randomUUID().toString(), "admin", new Date(), 'M', "1234",
				roleRepo.findByRoleCode("BRANCH_MANAGER"), bcryptEncoder.encode("12345"));
		if (customerRepo.findCustomerByName("admin") == null) {
			customerRepo.save(customer);
			return new ReturnHolder("Customer created ",null, HttpStatus.OK.value());
		} else {
			return new ReturnHolder("Customer already created ",null, HttpStatus.OK.value());
		}
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			List<SimpleGrantedAuthority> authorities = new ArrayList<>();
	        customerRepo.findCustomerByName(username).getRole().getRoleCode();
	        authorities.add(new SimpleGrantedAuthority("BRANCH_MANAGER"));
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password, authorities));
		} catch (DisabledException e) {
			logger.error("DisabledException",e);
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e ) {
			logger.error("BadCredentialsException",e);
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
	
}