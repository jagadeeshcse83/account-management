package com.jaga.customerservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.customerservice.dto.AccountCustomerDTO;
import com.jaga.customerservice.dto.ReturnHolder;
import com.jaga.customerservice.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@PostMapping("/customer")
	public ReturnHolder createCustomer(@RequestBody final AccountCustomerDTO accountCustomerDTO) {
		return customerService.createCustomer(accountCustomerDTO);
	}

	@DeleteMapping("/customer/{customerId}")
	public ReturnHolder deleteCustomer(@PathVariable("customerId") final String customerId) {
		System.out.println(customerId);
		return customerService.delete(customerId);
	}

	@PutMapping("/customer/{customerId}")
	public ReturnHolder updateCustomer(@RequestBody final AccountCustomerDTO accountCustomerDTO,
			@PathVariable("customerId") final String customerId) {
		return customerService.update(accountCustomerDTO, customerId);
	}

	@GetMapping("/customers")
	public ReturnHolder getAllCustomers(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "userName") String sortBy) {
		return customerService.getAllCustomers(pageNo, pageSize, sortBy);
	}
	

	@GetMapping("/customer/{userName}")
	public ReturnHolder getCustomerByByName(@PathVariable("userName") final String userName) {
		return customerService.getCustomerByName(userName);
	}
	
	
}
