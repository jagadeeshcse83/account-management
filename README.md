**Application name** : Customer account Management 

Created three Application (1 Application is for the **Service registory** and another 2 micro service applications for **Account and Customer** micro services) 


1. eurekaServer
1. customer-service
1. account-service

- Command to run the application 
mvn clean package spring-boot:run


Data base scripts 

User Application table data (Microservice 1)

| Column | Type | Remarks |
| ------ | ------ |------ |
| userId | Varchar | Primary key | 
| userName | Varchar | - | 
| dateOfBirth | Date | - | 
| gender | Char | Mandatory | 
| phoneNumber | Varchar | - | 

Role Application table data (Microservice 1)


| Column | Type | Remarks |
| ------ | ------ |------ |
| roleId | Varchar | Primary key | 
| roleName | Varchar | Unique | 
| roleCode | Varchar | Unique | 


Account Application table data (Microservice 2)

| Column | Type | Remarks |
| ------ | ------ |------ |
| accountId | Varchar | Primary key | 
| accountType | Varchar | - | 
| openDate | Date | - | 
| CustomerId | Varchar | Mandatory FK(User) | 
|  customerName | Varchar | - | 
|  branch | Varchar | - | 
|  minorIndicator | Char |  Tell whether the age of the holder is above 18 years. Derive it from the date of birth. | 


**Note : ** Application uses the postgresql 

** driver-class-name: org.postgresql.Driver
    url: jdbc:postgresql://localhost:5432/user-account-management
    username: postgres
    password: test**


- Exposed an end point /test-customer in customer Microservice to create the admin with manager role and password : 1234
- end point for admin creation
GET /test-customer HTTP/1.1
Host: localhost:8098
- Primary Keys are auto generated with UUID of string type
- Role are MASTER DATA hence need to create the sample records with role (manager and customer)
Need to excute the below sql script in role table
INSERT INTO ROLE (SELECT uuid_in(md5(random()::text || clock_timestamp()::text)::cstring),'BRANCH_MANAGER','Branch Manager');

INSERT INTO ROLE (SELECT uuid_in(md5(random()::text || clock_timestamp()::text)::cstring),'CUSTOMER','Customer');
- Use the generate end point to create the token http://localhost:8098/generate-token (username and password are need to generated the token)
- end point for token generation
POST /generate-token HTTP/1.1
Host: localhost:8098
Content-Type: application/json
{
	
	"username" : "jaga",
	"password" : "1234"
}
- All the end points to provide with a valid token (exclude : token generation and sample admin creation)



Application End points 

All the end points to provide with token : 
> Authorization: Bearer tokenid****

> Customer service end points****

_- POST http://localhost:8098/customer_

**Request 1**
POST /customer HTTP/1.1
Host: localhost:8098
Authorization: Bearer tokenId
Content-Type: application/json

{
   "userName":"Jagan",
   "roleCode":"BRANCH_MANAGER",
   "dateOfBirth":"20/12/1992",
   "gender":"m",
   "phoneNumber":"99999999"
}


**Response **
{
    "statusCode": 200,
    "result": {
        "userId": "a09b9f67-200f-4cfd-8eee-6e1a9c519809",
        "userName": "Jagan",
        "dateOfBirth": "1992-12-19T18:30:00.000+00:00",
        "gender": "m",
        "phoneNumber": "99999999",
        "role": {}
    },
    "message": "customer created successfully"
}

**Request 2**

POST /customer HTTP/1.1
Host: localhost:8098
Authorization: Bearer tokenId
Content-Type: application/json

{
   "userName":"Jagan",
   "roleCode":"BRANCH_MANAGER",
   "dateOfBirth":"20/12/1992",
   "phoneNumber":"99999999"
}

**Response 2**

{
    "statusCode": 400,
    "message": "Invalid gender. Valid roles are [M, F, O]"
}

_- PUT http://localhost:8098/customer/{customerId}_

**Request 1**

PUT /customer/3027dedd-4c57-459a-92f5-97fafad0159f HTTP/1.1
Host: localhost:8098
Authorization: Bearer tokenId
Content-Type: application/json
{
   "userName":"Jagan",
   "roleCode":"BRANCH_MANAGER",
   "dateOfBirth":"20/12/1992",
   "gender" : "m",
   "phoneNumber":"12312123"
}

**Response 1**
{
    "statusCode": 200,
    "message": "customer updated successfully"
}

> - DELETE http://localhost:8098/customer/{customerId}

**Request 1**

DELETE /customer/3027dedd-4c57-459a-92f5-97fafad0159f HTTP/1.1
Host: localhost:8098
Authorization: Bearer tokenId
**Response 1**

{
    "statusCode": 200,
    "message": "customer deleted successfully"
}

**Response 2**

{
    "statusCode": 400,
    "message": "customer not found in the system"
}

_- GET ALL http://localhost:8098/customers?pageNo=1&pageSize=1&sortBy=userName_

**Request 1**

GET /customers HTTP/1.1
Host: localhost:8098
Authorization: Bearer tokenId
**Response 1**

{
    "statusCode": 200,
    "result": [
        {
            "userId": "17e998ae-13b5-4fbe-bf66-a2a3ae2cc43d",
            "userName": "jagadeesh123",
            "dateOfBirth": "2020-12-19T18:30:00.000+00:00",
            "gender": "m",
            "phoneNumber": "123123123",
            "role": {}
        },
        {
            "userId": "a09b9f67-200f-4cfd-8eee-6e1a9c519809",
            "userName": "Jagan",
            "dateOfBirth": "1992-12-19T18:30:00.000+00:00",
            "gender": "m",
            "phoneNumber": "99999999",
            "role": {}
        }
    ],
    "message": ""
}

**Request 2**

GET /customers?pageNo=2 HTTP/1.1
Host: localhost:8098
Authorization: Bearer tokenId
**Response 2**

{
    "statusCode": 200,
    "result": [],
    "message": "No results found"
}





> Account service end points****

- POST http://localhost:9098/account

**Request : 1**
POST /account HTTP/1.1
Host: localhost:9098
Content-Type: application/json
Authorization: Bearer tokenId
{
    "accountType" : "SAVINGS",
    "branch" : "AP",
    "userName" : "jagadeesh123",
    "roleCode" : "CUSTOMER",
    "dateOfBirth" : "20/12/2020",
    "gender" : "m",
    "phoneNumber" :"123123123"
    
}

**Response : 1**

{
    "statusCode": 200,
    "message": "Account created successfully"
}


- PUT http://localhost:9098/account/4e2f548f-74f9-49b5-8ad4-8282a1737f14

**Request : 1**

PUT /account/4e2f548f-74f9-49b5-8ad4-8282a1737f14 HTTP/1.1
Host: localhost:9098
Content-Type: application/json
Authorization: Bearer tokenId
{
    "accountType" : "SAVINGS",
    "branch" : "AP",
    "userName" : "123123",
    "roleCode" : "CUSTOMER",
    "dateOfBirth" : "20/12/2020",
    "gender" : "m",
    "phoneNumber" :"123123123"
    
}

**Response : 1**

{
    "statusCode": 200,
    "message": "Account updated successfully"
}

- DELETE http://localhost:9098/account/4e2f548f-74f9-49b5-8ad4-8282a1737f14

**Request : 1**

DELETE /account/4e2f548f-74f9-49b5-8ad4-8282a1737f14 HTTP/1.1
Host: localhost:9098
Authorization: Bearer tokenId
**Response : 1**

{
    "statusCode": 200,
    "message": "Account deleted from the system"
}


- GET ALL http://localhost:9098/accounts?pageNo=1&pageSize=1&sortBy=branch

**Request : 1**

GET /accounts?pageNo=0&amp;pageSize=10&amp;sortBy=branch HTTP/1.1
Host: localhost:9098
Authorization: Bearer tokenId

**Response : 1**

{
    "statusCode": 200,
    "result": [
        {
            "accountId": "d79852c2-68eb-424c-832f-e0d541d5ffed",
            "accountType": "SAVINGS",
            "openDate": "2020-12-21T15:28:44.857+00:00",
            "branch": "AP",
            "minorIndicator": "N"
        }
    ],
    "message": ""
}

