package com.jaga.accountservice.validator;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.jaga.accountservice.dto.AccountCustomerDTO;
import com.jaga.accountservice.dto.ReturnHolder;

@Component
public class AccountValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		
	}
	
	List<String> accountTypes = Arrays.asList("SAVINGS","CURRENT","FIXED","JOINT");
	
	public ReturnHolder validateAccount(AccountCustomerDTO accountCustomerDTO) {
		ReturnHolder returnHolder = null;
		if (accountCustomerDTO == null) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid Data");
		} else if (accountCustomerDTO.getAccountType() == null || !accountTypes.contains(accountCustomerDTO.getAccountType())) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid account type specfied. Valid account types are " + accountTypes);
		} else if (accountCustomerDTO.getOpenDate()  != null && !isvalidDate(accountCustomerDTO.getOpenDate())) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid date formate for account open date. valid format dd/MM/yyyy ");
		} else if (accountCustomerDTO.getBranch() == null) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Invalid branch");
		} else {
			return returnHolder;
		}
	}
	
	private boolean isvalidDate(String openDate) {
		try {
			new SimpleDateFormat("dd/MM/yyyy").parse(openDate);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
}
