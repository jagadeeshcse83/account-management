package com.jaga.accountservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.jaga.accountservice.model.Account;

@Repository
public interface AccountRepo extends PagingAndSortingRepository<Account, String>{

	@Query(value = "SELECT * FROM account a WHERE a.account_type = ?1 and a.customer_id = ?2 ", nativeQuery = true)
	List<Account> findByAccountTypeAndCustomerId(String accountType, String customerId);
	
	@Query(value = "SELECT * FROM account a WHERE a.customer_id = ?1 ", nativeQuery = true)
	List<Account> findByCustomerId(String customerId);

	@Query(value = "SELECT * FROM account a WHERE a.account_id = ?1", nativeQuery = true)
	Account findByAccountId(String accountId);

}
