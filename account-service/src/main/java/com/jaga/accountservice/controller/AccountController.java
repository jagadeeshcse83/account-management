package com.jaga.accountservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jaga.accountservice.dto.AccountCustomerDTO;
import com.jaga.accountservice.dto.ReturnHolder;
import com.jaga.accountservice.service.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@PostMapping("/account")
	public ReturnHolder createAccount(@RequestBody AccountCustomerDTO accountCustomerDTO, @RequestHeader(value="Authorization") String token) {
		return accountService.createAccount(accountCustomerDTO, token);
	}
	
	
	@DeleteMapping("/account/{accountId}")
	public ReturnHolder deleteAccount(@PathVariable String accountId) {
		return accountService.deleteAccount(accountId);
	}
	
	@PutMapping("/account/{accountId}")
	public ReturnHolder updateCustomer(@RequestBody final AccountCustomerDTO accountCustomerDTO,
			@PathVariable("accountId") final String accountId) {
		return accountService.updateAccount(accountCustomerDTO, accountId);
	}
	
	
	@GetMapping("/accounts")
	public ReturnHolder getAllCustomers(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "branch") String sortBy) {
		return accountService.getAllAccounts(pageNo, pageSize, sortBy);
	}
	
	
}
