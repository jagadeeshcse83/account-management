package com.jaga.accountservice.service;

import com.jaga.accountservice.dto.AccountCustomerDTO;
import com.jaga.accountservice.dto.ReturnHolder;

public interface AccountService {

	ReturnHolder createAccount(final AccountCustomerDTO account, final String token);
	
	ReturnHolder updateAccount(final AccountCustomerDTO account, final String accountId);
	
	ReturnHolder deleteAccount(final String accountId);
	
	ReturnHolder getAllAccounts(final Integer pageNo,final Integer pageSize,final String sortBy);
}
