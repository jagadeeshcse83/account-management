package com.jaga.accountservice.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaga.accountservice.dto.ReturnHolder;
import com.jaga.accountservice.model.Customer;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private EurekaClient eurekaClient;
	
	private WebClient webClient;
	
	private static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

	public UserDetails loadUserByUsername(String username, String token) {
		ReturnHolder returnHolder = new ReturnHolder();
		ObjectMapper mapper = new ObjectMapper();
		InstanceInfo instanceInfo = eurekaClient.getApplication("customer-service").getInstances().get(0);
		webClient = WebClient.builder()
				.baseUrl("http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort())
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).build();
		returnHolder = webClient.get().uri("/customer/"+username)
				.header("Authorization", token)
				.retrieve()
				.bodyToMono(ReturnHolder.class).block();
		Customer customerDTO = mapper.convertValue(returnHolder.getResult(), Customer.class);
		if (customerDTO == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		SimpleGrantedAuthority authority = new SimpleGrantedAuthority(customerDTO.getRole().getRoleCode());
		List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		authorities.add(authority);
		return new User(customerDTO.getUserName(), customerDTO.getPassword(),
				authorities);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return null;
	}

}
