package com.jaga.accountservice.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaga.accountservice.dto.AccountCustomerDTO;
import com.jaga.accountservice.dto.ReturnHolder;
import com.jaga.accountservice.model.Account;
import com.jaga.accountservice.model.Customer;
import com.jaga.accountservice.repository.AccountRepo;
import com.jaga.accountservice.validator.AccountValidator;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

import reactor.core.publisher.Mono;

@Service
public class AccountServiceImpl implements AccountService{

	
	@Autowired
	private AccountRepo accountRepo;
	
	@Autowired
	private EurekaClient eurekaClient;
	
	private WebClient webClient;
	
	@Autowired
	AccountValidator accountValidator;
	
	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
	
	public ReturnHolder createAccount(final AccountCustomerDTO accountCustomerDTO, final String token){
		ReturnHolder returnHolder = accountValidator.validateAccount(accountCustomerDTO);
		try {
			if (returnHolder == null) {
				InstanceInfo instanceInfo = eurekaClient.getApplication("customer-service").getInstances().get(0);
				webClient = WebClient.builder()
						.baseUrl("http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort())
						.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).build();
				returnHolder = webClient.post().uri("/customer").contentType(MediaType.APPLICATION_JSON)
						.header("Authorization",token)
						.body(Mono.just(accountCustomerDTO), AccountCustomerDTO.class).retrieve()
						.bodyToMono(ReturnHolder.class).block();
				if (returnHolder.getStatusCode().equals(HttpStatus.OK.value())) {
					ObjectMapper mapper = new ObjectMapper();
					Customer customerDTO = mapper.convertValue(returnHolder.getResult(), Customer.class);
					if (customerDTO != null) {
						long diffInMillies = Math.abs(new Date().getTime() - customerDTO.getDateOfBirth().getTime());
						long noOfdays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
						List<Account> accounts = new ArrayList<>();
						if (accountCustomerDTO.getAccountType() == null) {
							accounts = accountRepo.findByCustomerId(customerDTO.getUserId());
						} else {
							accounts = accountRepo.findByAccountTypeAndCustomerId(accountCustomerDTO.getAccountType(),
									customerDTO.getUserId());
						}
						if (accounts != null && !accounts.isEmpty()) {
							return new ReturnHolder(HttpStatus.BAD_REQUEST.value(),
									"Account already existed with same customer ");
						} else {
							Account account = new Account(UUID.randomUUID().toString(),
									accountCustomerDTO.getAccountType(), new Date(), customerDTO, accountCustomerDTO.getBranch(),
									noOfdays > 6570l ? 'Y' : 'N');
							accountRepo.save(account);
							return new ReturnHolder(HttpStatus.OK.value(), "Account created successfully");
						}
					}
				} else {
					return returnHolder;
				}
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		return returnHolder;
	}

	@Override
	public ReturnHolder updateAccount(final AccountCustomerDTO accountCustomerDTO, final String accountId) {
		Account account = accountRepo.findByAccountId(accountId);
		ReturnHolder returnHolder = new ReturnHolder();
		if (account != null) {
			returnHolder = accountValidator.validateAccount(accountCustomerDTO);
			if (returnHolder == null) {
				account.setAccountType(accountCustomerDTO.getAccountType());
				account.setBranch(accountCustomerDTO.getBranch());
				accountRepo.save(account);
				return new ReturnHolder(HttpStatus.OK.value(), "Account updated successfully");
			} else  {
				return returnHolder;
			}
		} else {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Account not found with "+accountId);
		}
	}

	@Override
	public ReturnHolder deleteAccount(final String accountId) {
		try {
			Account account = accountRepo.findByAccountId(accountId);
			if (account != null) {
				accountRepo.deleteById(accountId);
				return new ReturnHolder(HttpStatus.OK.value(), "Account deleted from the system");
			} else {
				return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Account not found with "+accountId);
			}
		} catch (Exception e) {
			return new ReturnHolder(HttpStatus.BAD_REQUEST.value(), "Account not found with "+accountId);
		}
	}

	@Override
	public ReturnHolder getAllAccounts(final Integer pageNo,final Integer pageSize,final String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Account> pagedResult = accountRepo.findAll(paging);
        if(pagedResult.hasContent()) {
        	return new ReturnHolder("", pagedResult.getContent(), HttpStatus.OK.value());
        } else {
        	return new ReturnHolder("No results found", new ArrayList<>(), HttpStatus.OK.value());
        }
	}
	
}
