package com.jaga.accountservice.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="account")
public class Account {

	@Id
	private String accountId;
	
	private String accountType;
	
	private Date openDate;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
	private Customer customerId;
	
	//private String customerName;
	
	private String branch;
	
	private Character minorIndicator;

	public Account(String accountId, String accountType, Date openDate, Customer customerId, String branch,
			Character minorIndicator) {
		super();
		this.accountId = accountId;
		this.accountType = accountType;
		this.openDate = openDate;
		this.customerId = customerId;
		this.branch = branch;
		this.minorIndicator = minorIndicator;
	}
	
	public Account() {
		super();
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Character getMinorIndicator() {
		return minorIndicator;
	}

	public void setMinorIndicator(Character minorIndicator) {
		this.minorIndicator = minorIndicator;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", accountType=" + accountType + ", openDate=" + openDate
				+ ", customerId=" + customerId + ", branch=" + branch + ", minorIndicator=" + minorIndicator + "]";
	}
	
	
	
	
}
